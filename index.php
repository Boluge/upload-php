<!doctype html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<title>Upload Php</title>
	<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<div class="container">
		<h1>Upload PHP</h1>
		<hr>
		<form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>" enctype="multipart/form-data">	
			<p><input type="file" name="avatar"></p>
			<p><input type="submit" name="envoyer" value="Envoyer le fichier"></p>
		</form>
		<hr>
		<?php
			if (isset($_POST['envoyer'])){
				$dossier = 'upload/';
				$fichier = basename($_FILES['avatar']['name']);
				$taille_maxi = 90000000;
				$taille = filesize($_FILES['avatar']['tmp_name']);
				$extensions = array('.pdf', '.gif', '.jpg', '.jpeg');
				$extension = strrchr($_FILES['avatar']['name'], '.'); 
				function clear_dir($dir, $delete = false) {
					$dossier = $dir;
					$dir = opendir($dossier); 
					while($file = readdir($dir)) { 
						if(!in_array($file, array(".", ".."))){
							if(is_dir("$dossier/$file")) {
								clear_dir("$dossier/$file", true);
							} else {
								unlink("$dossier/$file");
							}
							 
							 
						}
					} 
					closedir($dir);
					 
					if($delete == true) {
						rmdir("$dossier/$file");
					}
				}
				//Début des vérifications de sécurité...
				if(!in_array($extension, $extensions)) //Si l'extension n'est pas dans le tableau
				{
					$erreur = 'Vous devez uploader un fichier de type pdf';
				}
				if($taille>$taille_maxi)
				{
					$erreur = 'Le fichier est trop gros...';
				}
				if(!isset($erreur)) //S'il n'y a pas d'erreur, on upload
				{
					//On formate le nom du fichier ici...
					$fichier = 'catalogue.pdf';
					if(move_uploaded_file($_FILES['avatar']['tmp_name'], $dossier . $fichier)) //Si la fonction renvoie TRUE, c'est que ça a fonctionné...
					{
						clear_dir(realpath(dirname(__FILE__) . "/catalogue"));
						echo 'Upload effectué avec succès & dossier catalogue vidé !';
					}
					else //Sinon (la fonction renvoie FALSE).
					{
						echo 'Echec de l\'upload !';
					}
				}
				else
				{
					echo $erreur;
				}
			}
		?>
		<?php
$nb_fichier = 0;
echo '<ul>';
if($dossier = opendir('./upload')){
	while(false !== ($fichier = readdir($dossier))){
		if($fichier != '.' && $fichier != '..' && $fichier != 'index.php'){
			$nb_fichier++; // On incrémente le compteur de 1
			echo '<li><a href="./upload/' . $fichier . '">' . $fichier . '</a></li>';
		} // On ferme le if (qui permet de ne pas afficher index.php, etc.)
  
	} // On termine la boucle
}
?>
	</div>
</body>
</html>